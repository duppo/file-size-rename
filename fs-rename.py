#!/usr/bin/python3

import os
import matplotlib.pyplot as plt

from argparse import ArgumentParser

if __name__ == "__main__":
    parser = ArgumentParser(description="Rank and rename files according to filesize.")
    parser.add_argument(
        "in_path", type=str,
        help="Main folder path (containing sub folders with images.)"
    )
    parser.add_argument(
        "-p", "--plot", action="store_true",
        help="Plot the filesize series."
    )
    args = parser.parse_args()

    for subdir, _, files in os.walk(args.in_path):
        print(" ")  # empty line for readability
        print("Opening directory: {0} ...".format(subdir))
        if not files:
            print("No files here.")
            continue

        sorted_files = []

        for f in files:
            f_path = os.path.join(subdir, f)

            if not f_path.endswith(".png"):
                continue

            f_size = os.path.getsize(f_path)

            print("{0} has size: {1}".format(f, f_size))

            if not sorted_files:
                # list is empty, the current file is the smallest.
                sorted_files.append((f_path, f_size))
            elif f_size >= os.path.getsize(sorted_files[-1][0]):
                # current file is bigger or equal compared to last in list
                sorted_files.append((f_path, f_size))
            else:
                # current file is smaller than last in list
                for sf in sorted_files:
                    # comparing to all files already added to the sorted list
                    sf_size = os.path.getsize(sf[0])

                    if f_size < sf_size:
                        # insert current file before sf in list
                        sf_i = sorted_files.index(sf)  # getting the index to split the list

                        first_half = sorted_files[:sf_i]
                        first_half.append((f_path, f_size))  # adding the file to the first half

                        second_half = sorted_files[sf_i:]
                        sorted_files = first_half + second_half  # putting the list back together
                        break  # sine we managed to insert the file, exit this loop

        if not args.plot:
            # rename the files
            print("...")  # empty line for readability
            print("Renamed files: ")
            for idx, sf in enumerate(sorted_files): # enumerate gives us the index of each list element
                new_name = sf[0].replace(".png", "-{0}.png".format(idx+1))
                os.rename(sf[0], new_name) # add idx+1 to the end (to start counting at 1)
                print(new_name)
        else:
            # plot sorted file data
            f_sizes = [f_tuple[1] for f_tuple in sorted_files]
            plt.plot(f_sizes)
            plt.ylabel("file size")
            plt.title(subdir)
            plt.show()
